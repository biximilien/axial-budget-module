package ca.rbgproductions.axial.modules.budget;

import java.util.ResourceBundle;

public class BudgetHTMLRenderer extends AbstractHTMLRenderer {

    private static final String THEAD_END = "</thead>";
    private static final String THEAD = "<thead id=\"budget-head\">";
    private static final String TBODY       = "<tbody id=\"budget-body\">".intern();
    private static final String TBODY_END   = "</tbody>".intern();

    private static final String TFOOT       = "<tfoot id=\"budget-foot\">".intern();
    private static final String TFOOT_END   = "</tfoot>".intern();

    private static final String TABLE       = "<table id=\"budget\" class=\"table table-striped table-hover\">".intern();
    private static final String TABLE_END   = "</table>".intern();

    private static final String TR          = "<tr>".intern();
    private static final String TR_END      = "</tr>".intern();

    private static final String TH          = "<th>".intern();
    private static final String TH_END      = "</th>".intern();

    private static final String TD_END      = "</td>".intern();
    private static final String TD          = "<td>".intern();

    private static final String NEWLINE     = "\n".intern();

    private static final String BUNDLE_NAME = "ca.rbgproductions.axial.modules.budget.messages".intern();

    private Budget  budget;
    public  Budget  getBudget()                 { return budget; }
    public  void    setBudget(Budget budget)    { this.budget = budget; }

    @Override
    public String render() {
        // Load resource bundle
        ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_NAME, budget.getLocale());

        // Use string builder for faster string operations
        StringBuilder budget = new StringBuilder();

        // Append HTML table entries
        appendBudget(bundle, budget);

        // Renders to a string object
        return budget.toString();
    }

    private void appendBudget(ResourceBundle bundle, StringBuilder builder) {
        appendTable(builder);
        appendTableHeader(bundle, builder);
        appendTableBody(bundle, builder);
        appendTableFooter(bundle, builder);
        appendTableEnd(builder);
    }

    private void appendTable(StringBuilder builder) {
        appendIndented(builder, 0, TABLE, NEWLINE);
    }

    private void appendTableEnd(StringBuilder builder) {
        appendIndented(builder, 0, TABLE_END, NEWLINE);
    }

    private void appendTableFooter(ResourceBundle bundle, StringBuilder builder) {
        // table foot
        appendIndented(builder, 1, TFOOT, NEWLINE);

        // total
        appendIndented(builder, 2, TR, NEWLINE);
        appendIndented(builder, 3, TH, bundle.getString("Budget.total"), TH_END, NEWLINE);
        appendIndented(builder, 3, TD, budget.getTotal(), TD_END, NEWLINE);
        appendIndented(builder, 2, TR_END, NEWLINE);

        // table foot end
        appendIndented(builder, 1, TFOOT_END, NEWLINE);
    }

    private void appendTableBody(ResourceBundle bundle, StringBuilder builder) {
        // table body
        appendIndented(builder, 1, TBODY, NEWLINE);

        appendIncomesHeader(bundle, builder);
        appendIncomes(builder);
        appendIncomesTotal(bundle, builder);

        appendExpensesHeader(bundle, builder);
        appendExpenses(builder);
        appendExpensesTotal(bundle, builder);

        // table body end
        appendIndented(builder, 1, TBODY_END, NEWLINE);
    }

    private void appendExpensesTotal(ResourceBundle bundle,
            StringBuilder builder) {
        // expenses total
        appendIndented(builder, 2, TR, NEWLINE);
        appendIndented(builder, 3, TH, bundle.getString("Budget.total_expenses"), TH_END, NEWLINE);
        appendIndented(builder, 3, TD, budget.getExpensesTotal(), TD_END, NEWLINE);
        appendIndented(builder, 2, TR_END, NEWLINE);
    }

    private void appendExpenses(StringBuilder builder) {
        for (Entry e : budget.getExpenses()) {
            appendIndented(builder, 2, TR, NEWLINE);
            appendIndented(builder, 3, TD, e.getInscription(), TD_END, NEWLINE);
            appendIndented(builder, 3, TD, e.getAmount(), TD_END, NEWLINE);
            appendIndented(builder, 2, TR_END, NEWLINE);
        }
    }

    private void appendExpensesHeader(ResourceBundle bundle, StringBuilder builder) {
        appendIndented(builder, 2, TR, NEWLINE);
        appendIndented(builder, 3, TH, TH_END, NEWLINE);
        appendIndented(builder, 3, TH, bundle.getString("Budget.expenses"), TH_END, NEWLINE);
        appendIndented(builder, 2, TR_END, NEWLINE);
    }

    private void appendIncomesTotal(ResourceBundle bundle, StringBuilder builder) {
        appendIndented(builder, 2, TR, NEWLINE);
        appendIndented(builder, 3, TH, bundle.getString("Budget.total_incomes"), TH_END, NEWLINE);
        appendIndented(builder, 3, TD, budget.getIncomesTotal(), TD_END, NEWLINE);
        appendIndented(builder, 2, TR_END, NEWLINE);
    }

    private void appendIncomes(StringBuilder builder) {
        for (Entry e : budget.getIncomes()) {
            appendIndented(builder, 2, TR, NEWLINE);
            appendIndented(builder, 3, TD, e.getInscription(), TD_END, NEWLINE);
            appendIndented(builder, 3, TD, e.getAmount(), TD_END, NEWLINE);
            appendIndented(builder, 2, TR_END, NEWLINE);
        }
    }

    private void appendIncomesHeader(ResourceBundle bundle,
            StringBuilder builder) {
        appendIndented(builder, 2, TR, NEWLINE);
        appendIndented(builder, 3, TH, TH_END, NEWLINE);
        appendIndented(builder, 3, TH, bundle.getString("Budget.incomes"), TH_END, NEWLINE);
        appendIndented(builder, 2, TR_END, NEWLINE);
    }

    private void appendTableHeader(ResourceBundle bundle, StringBuilder builder) {
        // table head
        appendIndented(builder, 1, THEAD, NEWLINE);

        // title
        appendIndented(builder, 2, TR, NEWLINE);
        appendIndented(builder, 3, TH, bundle.getString("Budget.budget"), TH_END, NEWLINE);
        appendIndented(builder, 3, TH, TH_END, NEWLINE);
        appendIndented(builder, 2, TR_END, NEWLINE);

        // table head end
        appendIndented(builder, 1, THEAD_END, NEWLINE);
    }

    private void appendIndented(StringBuilder builder, int indentationLevel, String... strings) {
        builder.append(indent(indentationLevel, strings));
    }

    private String indent(int indentationLevel, String... strings) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < indentationLevel; i++) {
            builder.append("  ");
        }
        for (String s : strings) {
            builder.append(s);
        }
        return builder.toString();
    }
}
