package ca.rbgproductions.axial.modules.budget;

public interface HTMLRenderer {
    
    String render();

}
