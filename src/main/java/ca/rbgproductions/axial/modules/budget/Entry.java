package ca.rbgproductions.axial.modules.budget;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;

public class Entry {

    private String     inscription;
    private BigDecimal amount;

    public Entry(String inscription, String amount) throws ParseException {
        setInscription(inscription);
        setAmount(amount);
    }

    public String getInscription() {
        return inscription;
    }

    public void setInscription(String inscription) {
        this.inscription = inscription;
    }

    public String getAmount() {
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        return formatter.format(amount);
    }

    public void setAmount(String amount) throws ParseException {
        if (amount != null) {
            NumberFormat formatter = NumberFormat.getCurrencyInstance();
            this.amount = new BigDecimal(formatter.parse(amount).toString());
        } else {
            this.amount = BigDecimal.ZERO;
        }
    }
}
