package ca.rbgproductions.axial.modules.budget;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Budget {

    private Map<Entry, String> incomes;

    public List<Entry> getIncomes() {
        return Collections.unmodifiableList(new ArrayList<Entry>(incomes
                .keySet()));
    }

    private Map<Entry, String> expenses;

    public List<Entry> getExpenses() {
        return Collections.unmodifiableList(new ArrayList<Entry>(expenses
                .keySet()));
    }

    private Locale locale;

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Budget(String languageCode, String countryCode) {
        setLocale(new Locale(languageCode, countryCode));
        incomes = new HashMap<Entry, String>();
        expenses = new HashMap<Entry, String>();
    }

    public Budget() {
        setLocale(Locale.getDefault());
        incomes = new HashMap<Entry, String>();
        expenses = new HashMap<Entry, String>();
    }

    public Budget(Locale locale) {
        this.setLocale(locale);
        incomes = new HashMap<Entry, String>();
        expenses = new HashMap<Entry, String>();
    }

    public void addIncomeEntry(String income, Entry entry) {
        incomes.put(entry, income);
    }

    public void addExpenseEntry(String expense, Entry entry) {
        expenses.put(entry, expense);
    }

    public void removeIncomeEntry(Entry entry) {
        incomes.remove(entry);
    }

    public void removeExpenseEntry(Entry entry) {
        expenses.remove(entry);
    }

    public boolean hasIncomeEntry(Entry entry) {
        return incomes.containsKey(entry);
    }

    public boolean hasExpenseEntry(Entry entry) {
        return expenses.containsKey(entry);
    }

    public String getTotal() {
        BigInteger totalIncomes = null;
        BigInteger totalExpenses = null;
        NumberFormat formatter = NumberFormat.getCurrencyInstance();

        try {
            totalIncomes = new BigInteger(formatter.parse(getIncomesTotal())
                    .toString());
        } catch (ParseException e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,
                    e.getLocalizedMessage());
        }

        try {
            totalExpenses = new BigInteger(formatter.parse(getExpensesTotal())
                    .toString());
        } catch (ParseException e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,
                    e.getLocalizedMessage());
        }

        return formatter.format(totalIncomes.subtract(totalExpenses));

    }

    public String getIncomesTotal() {
        BigDecimal amount = BigDecimal.ZERO;
        NumberFormat formatter = NumberFormat.getCurrencyInstance();

        for (Entry e : incomes.keySet()) {
            String addend = e.getAmount();
            try {
                amount = amount.add(new BigDecimal(formatter.parse(addend)
                        .toString()));
            } catch (ParseException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE,
                        ex.getLocalizedMessage());
            }
        }

        return formatter.format(amount);
    }

    public String getExpensesTotal() {
        BigDecimal amount = BigDecimal.ZERO;
        NumberFormat formatter = NumberFormat.getCurrencyInstance();

        for (Entry e : expenses.keySet()) {
            String addend = e.getAmount();
            try {
                amount = amount.add(new BigDecimal(formatter.parse(addend)
                        .toString()));
            } catch (ParseException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE,
                        ex.getLocalizedMessage());
            }
        }

        return formatter.format(amount);
    }

    public String render() {
        BudgetHTMLRenderer html = new BudgetHTMLRenderer();
        html.setBudget(this);
        return html.render();
    }
}
