package ca.rbgproductions.axial.modules.budget;

public abstract class AbstractHTMLRenderer implements HTMLRenderer {

    public AbstractHTMLRenderer() {

    }

    public abstract String render();

}
