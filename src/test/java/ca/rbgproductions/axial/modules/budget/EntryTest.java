package ca.rbgproductions.axial.modules.budget;

import java.text.ParseException;

import junit.framework.TestCase;

public class EntryTest extends TestCase {

	public void testEntry() throws ParseException {
		Entry entry = new Entry(null, null);
		assertNotNull(entry);
	}

	public void testGetInscription() throws ParseException {
		Entry entry = new Entry("Stuffing", "$500.00");
		assertEquals("Stuffing", entry.getInscription());
	}

	public void testSetInscription() throws ParseException {
		Entry entry = new Entry("Stuffing", "$500.00");
		Entry e = new Entry(null, null);
		e.setInscription("Stuffing");
		assertEquals(entry.getInscription(), e.getInscription());
	}

	public void testGetAmount() throws ParseException {
		Entry entry = new Entry("Stuffing", "$500.00");
		assertEquals("$500.00", entry.getAmount());
	}

	public void testSetAmount() throws ParseException {
		Entry entry = new Entry("Stuffing", "$500.00");
		Entry e = new Entry(null, null);
		e.setAmount("$500.00");
		assertEquals(entry.getAmount(), e.getAmount());
	}
}
