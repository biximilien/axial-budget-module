package ca.rbgproductions.axial.modules.budget;

import java.text.ParseException;
import java.util.Locale;

import junit.framework.TestCase;

public class BudgetTest extends TestCase {

	public void testBudget() {
		assertNotNull(new Budget(Locale.getDefault()));
	}

	public void testAddIncomeEntry() throws ParseException {
		Budget budget = new Budget(Locale.getDefault());
		Entry entry = new Entry("Stuffing", "$500.00");
		budget.addIncomeEntry("Assets", entry);
		assertTrue(budget.hasIncomeEntry(entry));
	}

	public void testAddExpenseEntry() throws ParseException {
		Budget budget = new Budget(Locale.getDefault());
		Entry entry = new Entry("Coffee", "$100.00");
		budget.addExpenseEntry("Services", entry);
		assertTrue(budget.hasExpenseEntry(entry));
	}

	public void testRemoveIncomeEntry() throws ParseException {
		Budget budget = new Budget(Locale.getDefault());
		Entry entry = new Entry("Foo bar stocks", "$10000.00");
		budget.addIncomeEntry("Capitalizations", entry);
		budget.removeIncomeEntry(entry);
		assertFalse(budget.hasIncomeEntry(entry));
	}

	public void testRemoveExpenseEntry() throws ParseException {
		Budget budget = new Budget(Locale.getDefault());
		Entry entry = new Entry("Ice cream", "$100.00");
		budget.addExpenseEntry("Necessities", entry);
		budget.removeExpenseEntry(entry);
		assertFalse(budget.hasExpenseEntry(entry));
	}

	public void testHasIncomeEntry() throws ParseException {
		Budget budget = new Budget(Locale.getDefault());
		Entry entry = new Entry("Snaffu", "$50.00");
		budget.addIncomeEntry("Assets", entry);
		assertTrue(budget.hasIncomeEntry(entry));
	}

	public void testHasExpenseEntry() throws ParseException {
		Budget budget = new Budget(Locale.getDefault());
		Entry entry = new Entry("Real panda furs", "$5000.00");
		budget.addExpenseEntry("Necessities", entry);
		assertTrue(budget.hasExpenseEntry(entry));
	}

	public void testGetTotal() throws ParseException {
		Budget budget = new Budget(Locale.getDefault());
		budget.addIncomeEntry("Assets", new Entry("Foo bar stocks", "$500.00"));
		budget.addIncomeEntry("Assets", new Entry("Snaffu stocks", "$500.00"));
		budget.addExpenseEntry("Stuff", new Entry("Ice cream", "$50.00"));
		budget.addExpenseEntry("Stuff", new Entry("Coffee", "$50.00"));
		assertEquals("$900.00", budget.getTotal());
	}

	public void testGetIncomesTotal() throws ParseException {
		Budget budget = new Budget(Locale.getDefault());
		budget.addIncomeEntry("Assets", new Entry("Foo bar stocks", "$500.00"));
		budget.addIncomeEntry("Assets", new Entry("Snaffu stocks", "$500.00"));
		assertEquals("$1,000.00", budget.getIncomesTotal());
	}

	public void testGetExpensesTotal() throws ParseException {
		Budget budget = new Budget(Locale.getDefault());
		budget.addExpenseEntry("Stuff", new Entry("Ice cream", "$50.00"));
		budget.addExpenseEntry("Stuff", new Entry("Coffee", "$50.00"));
		assertEquals("$100.00", budget.getExpensesTotal());
	}
}
